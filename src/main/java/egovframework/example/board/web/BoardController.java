package egovframework.example.board.web;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.management.RuntimeErrorException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.MultipartRequest;

import egovframework.com.cmm.EgovWebUtil;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.EgovProperties;
import egovframework.com.cmm.service.Globals;
import egovframework.example.board.service.BoardService;
import egovframework.example.board.service.BoardVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;


@Controller
public class BoardController {
	
	@Resource(name = "boardService")
	private BoardService boardService;
	
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService; // /spring에 있는 xml 설정 파일들의 값들을 들고올 수 있게 해줌.

	@RequestMapping(value = "/list.do")
	public String list(@ModelAttribute("boardVO") BoardVO boardVO, ModelMap model) throws Exception {
		

		/** EgovPropertyService.sample */
		boardVO.setPageUnit(propertiesService.getInt("pageUnit")); // properites -> context-properies.xml에 정의
		boardVO.setPageSize(propertiesService.getInt("pageSize"));

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(boardVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(boardVO.getPageUnit());
		paginationInfo.setPageSize(boardVO.getPageSize());

		boardVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		boardVO.setLastIndex(paginationInfo.getLastRecordIndex());
		boardVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		List<?> list = boardService.selectBoardList(boardVO);
		model.addAttribute("resultList", list);

		int totCnt = boardService.selectBoardListTotCnt(boardVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("searchKeyword", boardVO.getSearchKeyword());
		
		return "board/list";
	}
	
	
	@RequestMapping(value = "/mgmt.do", method = RequestMethod.GET)
	public String mgmt(@ModelAttribute("boardVO") BoardVO boardVO, ModelMap model, HttpServletRequest request) throws Exception {
		
		if ( boardVO.getIdx() == null ) { // 등록화면
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
			Calendar calendar = Calendar.getInstance();
			String strToday = simpleDateFormat.format(calendar.getTime());
			System.out.println("Today : " + strToday);
			
			boardVO.setIndate(strToday);
			boardVO.setWriter(request.getSession().getAttribute("userId").toString());
			boardVO.setWriterNm(request.getSession().getAttribute("userName").toString());
		
		} else { // 수정화면
			
			boardVO = boardService.selectBoard(boardVO);
			
			// 첨부파일 정보를 가져옴.
			BoardVO vo = boardService.selectAttach(boardVO);
			boardVO.setFilename(vo.getFilename());
			boardVO.setSeq(vo.getSeq()); 
			
			System.out.println("filename : " + boardVO.getFilename());
		}
		
		model.addAttribute("boardVO", boardVO);
				
		return "board/mgmt";
	}
	

	@RequestMapping(value = "/mgmtAdd.do", method = RequestMethod.POST)
	public String mgmtAdd(@ModelAttribute("boardVO") BoardVO boardVO, @RequestParam("mode") String mode, ModelMap model) throws Exception {
		
		if( "add".equals(mode) ) {
			
			boardService.insertBoard(boardVO);
			
		} else if( "mod".equals(mode) ) {
			
			boardService.updateBoard(boardVO);
			
		} else if( "del".equals(mode) ) {
			
			boardService.deleteBoard(boardVO);	
		}
		
		
		return "redirect:/list.do";
	}
	
	// 첨부파일 업로드 포함.
	@RequestMapping(value = "/mgmt.do", method = RequestMethod.POST)
	public String mgmt2(@ModelAttribute("boardVO") BoardVO boardVO, 
						@RequestParam("mode") String mode, 
						ModelMap model,
						HttpServletRequest request) throws Exception {
		
		MultipartHttpServletRequest mptRequest = null;
		try {
			mptRequest = (MultipartHttpServletRequest) request;
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		if( mptRequest != null) {
			
			Iterator fileIter = mptRequest.getFileNames();
			 
			while (fileIter.hasNext()) {
				MultipartFile mFile = mptRequest.getFile((String)fileIter.next());
			 
				if (mFile.getSize() > 0) {
					
					if( "mod".equals(mode) ) {
						fileDeleteSub(request);
					}
					
					HashMap map = EgovFileMngUtil.uploadFile(mFile);
			 
					System.out.println("[ "+Globals.FILE_PATH+" : "+map.get(Globals.FILE_PATH)+" ]");
					System.out.println("[ "+Globals.FILE_SIZE+" : "+map.get(Globals.FILE_SIZE)+" ]");
					System.out.println("[ "+Globals.ORIGIN_FILE_NM+" : "+map.get(Globals.ORIGIN_FILE_NM)+" ]");
					System.out.println("[ "+Globals.UPLOAD_FILE_NM+" : "+map.get(Globals.UPLOAD_FILE_NM)+" ]");
					System.out.println("[ "+Globals.FILE_EXT+" : "+map.get(Globals.FILE_EXT)+" ]");
					
					// boardVO.setFilename(map.get(Globals.UPLOAD_FILE_NM) + "." + map.get(Globals.FILE_EXT));
					boardVO.setFilename(map.get(Globals.UPLOAD_FILE_NM).toString());
				}
			}	
		}
		
		if( "add".equals(mode) ) {
			
			boardService.insertBoard(boardVO);
			
		} else if( "mod".equals(mode) ) {
			
			boardService.updateBoard(boardVO);
			
		} else if( "del".equals(mode) ) {
			
			boardService.deleteBoard(boardVO);	
		}
		
		
		return "redirect:/list.do";
	}
	
	private String getSaveLocation(HttpServletRequest request) {
		
		String uploadPath = EgovProperties.getProperty("Globals.fileStorePath");
		String attachPath = "";
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM");
		Calendar calendar = Calendar.getInstance();
		String folder = simpleDateFormat.format(calendar.getTime());
		attachPath = folder + "/";
		
		File cFile = new File(EgovWebUtil.filePathBlackList(uploadPath + attachPath));
		cFile.mkdir();
		
		System.out.println("UtilFile getSaveLocation path : " + uploadPath + attachPath);
		
		return uploadPath + attachPath;
	}
	
	@RequestMapping(value = "/fileAdd.do", method = RequestMethod.POST, produces="application/text;charset=utf8")
	@ResponseBody // return 할때 값 자체를 써줄 수 있음.
	public String fileupload(MultipartHttpServletRequest multipartRequest, HttpServletRequest request) throws Exception {
		
		// 순수하게  spring servlet에서 파일업로드 구현
		String path = "";
		String fileName = "";
		
		OutputStream out = null;
		PrintWriter printWriter = null;
		
		try {
			
			Iterator<String> itr = multipartRequest.getFileNames();
			
			while( itr.hasNext() ) {
				
				String formName = itr.next();
				MultipartFile uploadFile = multipartRequest.getFile(formName);
				
				try {
					
					fileName = uploadFile.getOriginalFilename();
					
					System.out.println("fileName : " + fileName); // filename test
					
					byte[] bytes = uploadFile.getBytes();
					path = getSaveLocation(multipartRequest);
					
					File file = new File(path);
					
					// 파일명이 중복으로 존재할 경우
					if( fileName != null && !"".equals(fileName) ) {
						if( file.exists() ) {
							
							fileName = System.currentTimeMillis() + "_" + fileName; 
							
							System.out.println("path : " + path); // path test
							System.out.println("fileName : " + fileName); // filename test
							
							file = new File(path + fileName);
						}
					}
					
					out = new FileOutputStream(file);
					out.write(bytes);
					
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				} finally {
					
					try {
						if( out != null ) {
							
							out.close();
						}
						if( printWriter != null ) {
							
							printWriter.close();
						}
						
					} catch (IOException e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
		// 멀티파일 세팅 (옛날 방식)
		// 이 소스를 사용할 경우 web.xml multipart filter 부분 주석을 제거해야함.
		/*MultipartRequest parser = null;
		int maxPostSize = 10 * 1024 * 1024;
		String rootPath = getSaveLocation(request);
		
		try {
			
			File dir = new File(rootPath);
			dir.mkdirs();
			parser = new MultipartRequest(request, rootPath, maxPostSize, "utf-8", new com.oreilly.servlet.multipart.DefaultFileRenamePolicy());
			
			Enumeration formNames = null;
			String formName = "";
			formName = parser.getFileNames();
			
			while( formNames.hasMoreElements() ) {
				
				formName = (String)formNames.nextElement();
				
				String filename = (parser.getFilesystemName(formName) == null )? "" : parser.getFilesystemName(formName);
				
				fileName = filename;
			}
			if( !"".equals(fileName) ) {
				path = rootPath;
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException();
		}*/
		
		System.out.println("업로드 경로 : " + (path + fileName));
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM");
		Calendar calendar = Calendar.getInstance();
		String folder = simpleDateFormat.format(calendar.getTime());
		
		return folder + "/" + fileName;
	}
	
	@RequestMapping(value = "/fileGet.do", method = RequestMethod.GET)
	public void fileDownload(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String stordFilePath = EgovProperties.getProperty("Globals.fileStorePath");
		 
		// 저장된 파일명
		String filename = request.getParameter("filename");
		// 첨부된 원 파일명
		String original = request.getParameter("original");
		 
//		if ("".equals(original)) {
			original = filename;
//		}
		 
		request.setAttribute("downFile", stordFilePath + filename);
		request.setAttribute("orginFile", original);
		
		System.out.println("download : " + (stordFilePath + filename));
		 
		EgovFileMngUtil.downFile(request, response);
	}
	
	@RequestMapping(value = "/fileDel.do", method = RequestMethod.GET)
	public String fileDelete(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String idx =  fileDeleteSub(request);
		
		return "redirect:/view.do?idx=" + idx;
		
	}
	
	public String fileDeleteSub(HttpServletRequest request) throws Exception {
		
		String stordFilePath = EgovProperties.getProperty("Globals.fileStorePath");
		 
		// 저장된 파일명
		String filename = request.getParameter("filename");
		 
		File file = new File(stordFilePath + filename);
		
		if (file.exists()) {
			file.delete();
		}

		// 첨부파일 데이터 삭제
		String idx = request.getParameter("idx");
		String seq = request.getParameter("seq");
		
		BoardVO vo = new BoardVO();
		vo.setIdx(idx);
		vo.setSeq(seq);
		
		boardService.deleteAttach(vo);
		
		return idx;
	}
	
	
	/*
		ModelAttribute는 Client단에서 Server단으로 전송하는 메시지	(client -> server)
		model은 Server단에서 Client단으로 전송하는 메시지				(server -> client)
	*/
	@RequestMapping(value = "/view.do")
	public String view(@ModelAttribute("boardVO") BoardVO boardVO, ModelMap model) throws Exception {
		
		System.out.println("idx : " + boardVO.getIdx());
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		Calendar calendar = Calendar.getInstance();
		String strToday = simpleDateFormat.format(calendar.getTime());
		
		boardService.updateBoardCount(boardVO);
		
		// 게시판 정보를 가져옴.
		boardVO = boardService.selectBoard(boardVO);
		
		// 첨부파일 정보를 가져옴.
		BoardVO vo = boardService.selectAttach(boardVO);
		boardVO.setFilename(vo.getFilename());
		boardVO.setSeq(vo.getSeq());
		
		// 댓글 정보를 가져옴.
		List<?> list = boardService.selectReplyList(boardVO);
		
		model.addAttribute("boardVO", boardVO);
		model.addAttribute("resultList", list);
		model.addAttribute("strToday", strToday);
		
		return "board/view";
	}
	
	@RequestMapping(value = "/reply.do", method = RequestMethod.POST)
	public String reply(@ModelAttribute("boardVO") BoardVO boardVO, ModelMap model) throws Exception {
		
		/*System.out.println("idx : " + boardVO.getIdx());*/
		
		boardService.insertReply(boardVO);
		
		return "redirect:/view.do?idx=" + boardVO.getIdx();
	}
	
	@RequestMapping(value = "/login.do")
	public String login(@RequestParam("user_id") String user_id, @RequestParam("password") String password,
			ModelMap model, HttpServletRequest request) throws Exception {
		
//		String user_id = request.getParameter("user_id");
//		String password = request.getParameter("password");
		
		System.out.println("user_id : " + user_id);
		System.out.println("password : " + password);
		
		BoardVO boardVO = new BoardVO();
		boardVO.setUserId(user_id);
		boardVO.setPassword(password);
		String user_name = boardService.selectLoginCheck(boardVO);
		
		if( user_name != null && !"".equals(user_name) ) {
			request.getSession().setAttribute("userId", user_id);
			request.getSession().setAttribute("userName", user_name);
		} else {
			request.getSession().setAttribute("userId", "");
			request.getSession().setAttribute("userName", "");
			model.addAttribute("msg","사용자 정보가 올바르지 않습니다.");
		}
				
		return "redirect:/list.do";
	}
	
	@RequestMapping(value = "/logout.do")
	public String logout(ModelMap model, HttpServletRequest request) throws Exception {
		
		request.getSession().invalidate();
				
		return "redirect:/list.do"; // redirect는 앞 페이지 정보를 모두 '초기화'시키고 페이지를 호출함.
	}
	
}
