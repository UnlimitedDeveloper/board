<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%

	request.setCharacterEncoding("utf-8");

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
	<title>Bootstrap Example</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width-device-width, initial-scale-1">
	<link rel="stylesheet" href="<c:url value='/css/bootstrap/css/bootstrap.min.css'/>">
	<script src="<c:url value='/js/jquery-3.4.0.min.js'/>"></script>
	<script src="<c:url value='/css/bootstrap/js/bootstrap.min.js'/> "></script>
	<script type="text/javaScript" language="javascript" defer="defer">
		
		$(document).ready(function() {
			
			$('#idx').attr("readonly", true);
			$('#writerNm').attr("readonly", true);
			$('#indate').attr("readonly", true);
		})
	
		function cancel() {
			
			location.href = "<c:url value='/list.do'/>";
		}
		
		function add() {
			
			if( $('#title').val() == '' ) {
				alert("제목을 입력하세요.");
				$('#title').focus();
				
				return;
			}
			if( $('#contents').val() == '' ) {
				alert("내용을 입력하세요.");
				$('#contents').focus();
				
				return;
			}
			if( !confirm("등록하시겠습니까?") ) {
				return;
			}
			
			document.form1.action = "<c:url value='/mgmt.do'/>?mode=add";
			document.form1.submit();
		}
		
		function add2() { /* 파일부터 올리고 데이터를 올림. */
			if( $('#title').val() == '' ) {
				alert("제목을 입력하세요.");
				$('#title').focus();
				
				return;
			}
			if( $('#contents').val() == '' ) {
				alert("내용을 입력하세요.");
				$('#contents').focus();
				
				return;
			}
			if( !confirm("등록하시겠습니까?") ) {
				return;
			}
			
			if( $('#file').val() != '' ) {
				var formData = new FormData();
				formData.append("file", $("input[name=file]")[0].files[0]);
				
				$.ajax({
					url : "<c:url value='/fileAdd.do'/>", /* 파일을 업로드하고 이름을 리턴받아오기 위한 작업 */
					processData : false,
					contentType : false,
					method : "POST",
					cache : false,
					data : formData
				})
				.done(function( data ) { /* fileupload() 함수 리턴값이 있으면 */
					if( data.indexOf("에러") >= 0 ) {
						return;
					}
					if( data != "") {
						$('#filename').val(data);
						document.form1.action = "<c:url value='/mgmtAdd.do'/>?mode=add";
						document.form1.submit();
					}
				})
				.fail(function( jqXHR, textStatus, errorThrown ) {
					alert("오류" + errorThrown);
				});
				
			} else {
				document.form1.action = "<c:url value='/mgmtAdd.do'/>?mode=add";
				document.form1.submit();
			}			
		}
		
		function mod() {
			
			if( $('#title').val() == '' ) {
				alert("제목을 입력하세요.");
				$('#title').focus();
				
				return;
			}
			if( $('#contents').val() == '' ) {
				alert("내용을 입력하세요.");
				$('#contents').focus();
				
				return;
			}
			if( !confirm("수정하시겠습니까?") ) {
				return;
			}
			
			document.form1.action = "<c:url value='/mgmt.do'/>?mode=mod";
			document.form1.submit();
		}
		
	</script>
</head>
<body>
	
	<div class="container">
	
		<h1>
			<c:if test="${empty boardVO.idx }">등록</c:if>
			<c:if test="${!empty boardVO.idx }">수정</c:if> 화면
		</h1>
		
		<div class="panel panel-default">
		  <div class="panel-heading">
			${sessionScope.userName }님 환영합니다.
			<button type="button" class="btn btn-default" onclick="out();">로그아웃</button>
		  </div>
		  
		  <div class="panel-body">
		  	<form id="form1" name="form1" class="form-horizontal" method="post" enctype="multipart/form-data" action="">
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="idx">게시물아이디 : </label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="idx" name="idx" placeholder="자동발번" value="${boardVO.idx }">
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="title">제목 : </label>
			    <div class="col-sm-10"> 
			      <input type="text" class="form-control" id="title" name="title" placeholder="제목을 입력하세요." maxlength="100" value="${boardVO.title }" >
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="writer/indate">등록자 / 등록일 :</label>
			    <div class="col-sm-10"> 
			      <input type="hidden" class="form-control" id="writer" name="writer" maxlength="15" style="float:left; width:40%;" value="${boardVO.writer }">
			      <input type="text" class="form-control" id="writerNm" name="writerNm" placeholder="등록자를 입력하세요." maxlength="15" style="float:left; width:40%;" value="${boardVO.writerNm }">
			      <input type="text" class="form-control" id="indate" name="indate" placeholder="등록일을 입력하세요." maxlength="10" style="width:40%;" value="${boardVO.indate }">
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="contents">내용 : </label>
			    <div class="col-sm-10"> 
			      <textarea class="form-control" rows="5" id="contents" name="contents" maxlength="1000" placeholder="1000자 이내로 입력하세요.">${boardVO.contents }</textarea>
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="filename">첨부파일 : </label>
			    <div class="col-sm-10 control-label" style="text-align: left;">
					<c:if test="${boardVO.filename != null }">
						<c:out value="${boardVO.filename }" escapeXml="false"/> 
					</c:if>
			      	<input type="file" class="form-control" id="file" name="file" />
			    </div>
			  </div>
			  <input type="hidden" id="seq" name="seq" value="${boardVO.seq }"/>
			  <input type="hidden" id="filename" name="filename" value="${boardVO.filename }" />
			</form>
		  </div>
		  
		  <div class="panel-footer">
		  	<c:if test="${empty boardVO.idx }">
		  		<button type="button" class="btn btn-default" onclick="add();">등록</button>
		  	</c:if>
		  	<c:if test="${!empty boardVO.idx }">
		  		<button type="button" class="btn btn-default" onclick="mod();">수정</button>
		  	</c:if>
		  	<button type="button" class="btn btn-default" onclick="cancel();">취소</button>
		  </div>
		  
		</div>
		
	</div>
	
</body>
</html>