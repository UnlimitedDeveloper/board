<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
	pageContext.setAttribute("crcn", "\r\n");
	pageContext.setAttribute("br", "<br />");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
	<title>Bootstrap Example</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width-device-width, initial-scale-1">
	<link rel="stylesheet" href="<c:url value='/css/bootstrap/css/bootstrap.min.css'/>">
	<script src="<c:url value='/js/jquery-3.4.0.min.js'/>"></script>
	<script src="<c:url value='/css/bootstrap/js/bootstrap.min.js'/> "></script>
	<script type="text/javaScript" language="javascript" defer="defer">
		
		function list() {
			
			location.href = "<c:url value='/list.do'/>";
		}
		
		function add() {
			
			if( $('#writer').val() == '' ) {
				alert("작성자를 입력하세요.");
				$('#writer').focus();
				
				return;
			}
			if( $('#reply').val() == '' ) {
				alert("댓글 내용을 입력하세요.");
				$('#reply').focus();
				
				return;
			}
			if( !confirm("댓글을 등록하시겠습니까?") ) {
				return;
			}
			
			document.form2.action = "<c:url value='/reply.do'/>";
			document.form2.submit();
		}
		
		function mod() {
			
			location.href = "<c:url value='/mgmt.do'/>?idx=" + ${boardVO.idx};
		}
		
		function del() {
			
			var cnt = ${fn:length(resultList)};
			
			if( cnt > 0 ) {
				alert("댓글이 존재하는 게시글은 삭제할 수 없습니다.");
				return;
			}
			
			if( !confirm("이 글을 삭제하시겠습니까?") ) {
				return;
			}
			
			document.form1.action = "<c:url value='/mgmt.do'/>?mode=del&idx=" + ${boardVO.idx};
			document.form1.submit();
		}
	
		function getfile(filename) {
			
			location.href = "<c:url value='/fileGet.do'/>?filename=" + filename;
		}
		
		function delfile(seq,filename) {
			
			if( !confirm("첨부 파일을 삭제하시겠습니까?") ) {
				return;
			}
			
			location.href = "<c:url value='/fileDel.do'/>?idx=" + ${boardVO.idx} +"&seq=" + seq + "&filename=" + filename;
		}
		
	</script>
</head>
<body>
	
	<div class="container">
	
		<h1>상세 화면</h1>
		
		<div class="panel panel-default">
		  <div class="panel-heading">
		  	<c:if test="${sessionScope != null && sessionScope.userId != null && sessionScope.userId != '' }">
				${sessionScope.userName }님 환영합니다.
				<button type="button" class="btn btn-default" onclick="out();">로그아웃</button>
			</c:if>
		  </div>
		  
		  <div class="panel-body">
		  	<form id="form1" name="form1" class="form-horizontal" method="post" action="">
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="">게시물아이디 : </label>
			    <div class="col-sm-10 control-label" style="text-align: left;">
			     	<c:out value="${boardVO.idx }"/>
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="">제목 : </label>
			    <div class="col-sm-10 control-label" style="text-align: left;">
			     	<c:out value="${boardVO.title }"/>
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="">등록자 / 등록일 :</label>
			    <div class="col-sm-10 control-label" style="text-align: left;">
			     	<c:out value="${boardVO.writerNm }"/> /
			     	<c:out value="${fn:substring(boardVO.indate,0,fn:length(boardVO.indate)-2) }"/>
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="">내용 : </label>
			    <div class="col-sm-10 control-label" style="text-align: left;">
			     	<c:out value="${fn:replace(boardVO.contents, crcn, br) }" escapeXml="false"/>
			    </div>
			  </div>
			</form>
		  </div>
		  
		  <div class="panel-footer">
		  	<c:if test="${!empty sessionScope.userId && sessionScope.userId == boardVO.writer }">
			  	<button type="button" class="btn btn-default" onclick="mod();">수정</button>
			  	<button type="button" class="btn btn-default" onclick="del();">삭제</button>
		  	</c:if>
		  	<button type="button" class="btn btn-default" onclick="list();">목록</button>
		  </div>
		  
		</div>
		
		<div class="well well-sm">
		  <table class="table">
		    <tbody>
		    	<div class="form-group">
			    	<label class="control-label col-sm-2" for="">파일 : </label>
			    	<c:if test="${boardVO.filename != null }">
				    	<div class="col-sm-10 control-label" style="text-align: left;">
				     		<a href="javascript:getfile('${boardVO.filename }');">
				     			<c:out value="${boardVO.filename }" escapeXml="false"/> 
				     		</a>
				     			&nbsp;
			     		<c:if test="${sessionScope != null && sessionScope.userId != null && sessionScope.userId != '' && boardVO.filename != null }">
				     		<a href="javascript:delfile('${boardVO.seq }','${boardVO.filename }');">
				     			<span class="glyphicon glyphicon-remove"></span>
				     		</a>
				     	</c:if>
				    	</div>
				    </c:if>
				    <c:if test="${boardVO.filename == null }">
				    	<label class="control-label" for="">
				    		첨부된 파일이 없습니다.
				    	</label>
				    </c:if>
			  </div>
		    </tbody>
		   </table>
		</div>
		
		<div class="well well-lg">
		  	<form id="form2" name="form2" class="form-horizontal" method="post" action="">
		  	  <div class="form-group">
			    <label class="control-label col-sm-2" for="writer/indate">작성자 :</label>
			    <div class="col-sm-10 control-label" style="text-align: left;">
			    	<input type="hidden" name="idx" value="${boardVO.idx }">
					<input type="text" class="form-control" id="writer" name="writer" placeholder="작성자를 입력하세요." maxlength="15" style="float:left; width:40%;" value="${sessionScope.userName }">
			      	<%-- <input type="text" class="form-control" id="indate" name="indate" placeholder="작성일을 입력하세요." maxlength="10" style="width:40%;" value="${strToday }" readonly> --%>
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="reply">댓글 : </label>
			    <div class="col-sm-10"> 
			      <textarea class="form-control" rows="3" id="reply" name="reply" maxlength="300" placeholder="300자 이내로 입력하세요."></textarea>
			    </div>
			  </div>
			  <button type="button" class="btn btn-default" onclick="add();">등록</button>
			  * 댓글을 수정이나 삭제를 할 수 없습니다.
		  	</form>
		</div>
		
		<!-- <div class="well well-sm">작성자 / 작성일 <br /> 내용</div> -->
		
		<div class="well well-sm">
		  <table class="table">
		    <tbody>
				<c:forEach var="result" items="${resultList}" varStatus="status">
			      <tr style="height:50px;">
			        <td style="width:20%;"><c:out value="${result.writer }"/></td>
			        <%-- <td style="width:60%;"><c:out value="${result.reply }"/></td> --%>
			        <td><c:out value="${fn:replace(result.reply, crcn, br) }" escapeXml="false"/></td>
			        <%-- <td><c:out value="${result.indate }"/></td> --%>
			        <td><fmt:formatDate value="${result.indate }" pattern="yyyy-MM-dd HH:mm:ss" /></td>
			      </tr>
		    	</c:forEach>
		    </tbody>
		   </table>
		</div>
		
	</div>
	
</body>
</html>